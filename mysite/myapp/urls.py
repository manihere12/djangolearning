from django.config.urls import pattern, url, include
from django.contrib import admin

urlpatterns = [
	url(r'^admin/$', admin.site.urls),
]