from __future__ import unicode_literals

from django.db import models

class Dreamreal(models.Model):
	website = models.CharField(max_length=30)
	mail = models.CharField(max_length=30)
	name = models.CharField(max_length=30)
	phonenumber = models.IntegerField()
	online = models.ForeignKey('Online', default=1)

	class Meta:
		db_table = 'dreamreal'

class Online(models.Model):
	domain = models.CharField(max_length=30)

	class Meta():
		db_table = 'online'

class Profile(models.Model):
	name = models.CharField(max_length=30)
	picture = models.ImageField(upload_to='pictures')

	class Meta():
		db_table = 'profile'
			
