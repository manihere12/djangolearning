from django.conf.urls import url, include
from django.contrib import admin
from myapp.views import *
from django.views.generic import TemplateView, ListView
from django.views.decorators.cache import cache_page
from django.conf import settings
from myapp.feeds import *
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.contrib.staticfiles.urls import staticfiles_urlpatterns




urlpatterns = [
url(r'^admin/', include(admin.site.urls)),
url(r'^hello/', hello, name='hello'),
url(r'^hello2/(?P<Name>\w+)/', hello2, name='hello2'),
url(r'^morning/', morning, name='morning'),
url(r'^article/(\d+)', article, name='article'), 
# The following url caches the page for 5 minutes. See settings.py
url(r'^articles/(\d{2})/(\d{4})', cache_page(60 * 5)(articles), name='articles'),
url(r'^time/', timenow, name='time'),
url(r'^crudops/', crudops, name='crudops'),
url(r'^datamanipulation/', datamanipulation, name='datamanipulation'),
url(r'^sendmail/', sendmail, name='sendmail'),
url(r'^sendmassmail/', sendmassmail, name='sendmassmail'),
url(r'^mailadmins/', mailadmins, name = 'mailadmins'),
url(r'^mailmanagers/', mailmanagers, name = 'mailmanagers'),
url(r'^static/', StaticView.as_view(), name='static'),
url(r'^staticurlonly/', TemplateView.as_view(template_name='staticurlonly.html')),
url(r'^sendhtmlemailnew/(?P<emailto>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/', sendhtmlemailnew, name='sendhtmlemailnew'),
url(r'^sendhtmlemailold/(?P<emailto>[\w.%+-]+@[A-Za-z0-9.%+-]+\.[A-Za-z]{2,4})/', sendhtmlemailold, name='sendhtmlemailold'),
# url(r'^dreamreals/', ListView.as_view(model='Dreamreal',template_name='dreamreal_list.html'), name='dreamreals'),
url(r'^dreamreals/',ListView.as_view(model=Dreamreal,template_name="dreamreal_list.html")),
url(r'^connection/', TemplateView.as_view(template_name='login.html'),name='connection'),
url(r'^login/', login, name='login'),
url(r'^profile/', TemplateView.as_view(template_name='saveprofile.html'), name='profile'),
url(r'^saveprofile/', Saveprofile, name='saveprofile'),
url(r'^sessionconnection/', formview, name='sessionconnection'),
url(r'^sessionlogin/', sessionlogin, name='sessionlogin'),
url(r'^logout/', logout, name='logout'),
url(r'^comments/', include('django_comments.urls'), name = 'comments'),
url(r'^latestcomments/', DreamrealCommentsFeed, name='latestcomments'),
url(r'^comment/(?P<object_pk>[\w+])', comment, name='comment'),
url(r'^%s/' %settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
url(r'^dreamreal/', dreamreal, name='dreamreal'),
]

urlpatterns += staticfiles_urlpatterns()