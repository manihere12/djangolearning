from django.contrib.syndication.views import Feed
from django_comments.models import Comment
from django.core.urlresolvers import reverse
from django.utils.deprecation import MiddlewareMixin


class DreamrealCommentsFeed(MiddlewareMixin):
	title = "Dreanreal's comments feeds"
	link = '/drcomments/'
	description = 'Updates on new comments on Dreamreal'
	
	def items(self):
		return Comment.objects.all().order_by('-submit_date')[:5]

	def item_title(self, item):
		return item.user_name

	def item_description(self, item):
		return item.comment

	def item_link(self, item):
		return reverse('comment', args=[item.pk])


