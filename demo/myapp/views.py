from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
import datetime
from myapp.models import *
from django.core.urlresolvers import reverse
from django.core.mail import send_mail, send_mass_mail, mail_admins, mail_managers,EmailMessage
from django.views.generic import TemplateView
from myapp.forms import *
from django.template import RequestContext
from django_comments.models import Comment
from myapp.feeds import *



def hello(request):
	today = datetime.datetime.now().date()
	daysofweek = ['Mon','Tues','Wed','Thurs','Fri','Sat','Sun']	
	# return redirect("http://www.djangoproject.com")
	return render(request, 'hello.html', {"today" : today})
	# text = "<h1> Welcome to my new app</h1>"
	# return HttpResponse(text)

def sendmail(request):
	res = send_mail(
		'Test Mail',
		'Sending a test mail',
		'admin@django.com',
		['manish.panwar40@gmail.com'],
		)
	return HttpResponse(res)

def sendmassmail(request):
	msg1 = ('Mass Mail 1','Sending first mail of the chain', 'admin@django.com',['manish.panwar40@gmail.com'])
	msg2 = ('Mass Mail 2', 'Sending 2nd mail', 'admin@django.com',['manihere@gmail.com'])
	res = send_mass_mail((msg1, msg2), fail_silently= False)
	return HttpResponse(res)	

def mailadmins(request):
	res = mail_admins('Mail to admins','Site is going Live!')
	return HttpResponse('Mail sent to Admins')

def mailmanagers(request):
	res = mail_managers('Mail to Managers', 'Calculate Profit Percentage')
	return HttpResponse('Mail sent to Managers')

# Send HTML Email for django > 1.7
def sendhtmlemailnew(request, emailto):
	res = send_mail('Sending HTML mail', 'This is the mail body', 'admin@django.com',[emailto], 
		html_message='<strong>This is a html content. </strong>')
	return HttpResponse('%s' %res)

# Send HTML Email For django < 1.7
def sendhtmlemailold(request,emailto): 
	html_content = '<strong>This is an HTML content.</strong>'
	email = EmailMessage('Sending HTML mail',html_content, 'admin@django.com',[emailto,'manihere@gmail.com'])
	email.content_subtype = 'html'
	email.attach_file('myapp/views.py')
	res = email.send()
	return HttpResponse('%s' %res)


def hello2(request, Name):
	dreamreal = Dreamreal.objects.get(name = Name)
	return render(request, 'hello2.html', locals())
	# return render (request, 'hello2.html')

def morning(request):
	text = '<h1> Good Morning</h1>'
	return HttpResponse(text)

def article(request, number):
	text = '<h1> You are reading article #%s' %number
	return HttpResponseRedirect(reverse('articles',args=('02','2012')))
	# return HttpResponse(text)
	# return redirect('articles', year="2012", month="02")
	# url = reverse('articles', kwargs ={'month':'02', 'year':'2012'})
	# return HttpResponse(url)

def articles(request, month, year):
	text = "<h1> Displaying articles of(mm/yyyy): %s/%s" %(month,year)
	return HttpResponse(text)

def timenow(request):
	time = datetime.datetime.now()
	text = "Time is: %s" %time
	return HttpResponse(text)

def crudops(request):
	dreamreal=Dreamreal(website="www.polo.com", mail="sorex@polo.com",
	name="sorex", phonenumber="002376970")
	dreamreal.save()

	objects = Dreamreal.objects.all()
	res ='Printing all Dreamreal entries in the DB : <br>'
	for elt in objects:
		res+=elt.name+"<br>"
	sorex = Dreamreal.objects.get(name="sorex")
	res += 'Printing One entry <br>'
	res += sorex.name

	res += '<br> Deleting an entry <br>'
	sorex.delete()

	dreamreal=Dreamreal(website="www.polo.com", mail="sorex@polo.com", name="sorex", phonenumber="002376970")
	dreamreal.save()
	res += 'Updating entry<br>'
	dreamreal = Dreamreal.objects.get(name='sorex')
	dreamreal.name = 'thierry'
	dreamreal.save()
	return HttpResponse(res)

def datamanipulation(request):
	res = ""
	qs = Dreamreal.objects.get(name="sorex")
	if len(qs) == 0:
		res = "No results found"
	else:
		res += "Found %s results <br>" %len(qs)
		# Order by
		qs = Dreamreal.objects.order_by(name)
		for obj in qs:
			res += obj.name
	return HttpResponse(res) 

class StaticView(TemplateView):
	template_name= 'static.html'

def login(request):
	username = 'not logged in'
	if request.method == 'POST':
		MyLoginForm = LoginForm(request.POST)
		if MyLoginForm.is_valid():
			username = MyLoginForm.cleaned_data['username']

	return render(request, 'loggedin.html', {'username' : username})

# def Saveprofile(request):	
# 	saved = False
# 	if request.method == 'POST':
# 		MyProfileForm = ProfileForm(request.POST, request.FILES)	
# 		if MyProfileForm.is_valid():
# 			myprofile = Profile()
# 			myprofile.name = MyProfileForm.cleaned_data['name']
# 			myprofile.picture = MyProfileForm.cleaned_data['picture']
# 			myprofile.save()
# 			saved = True
# 	else:
# 		MyProfileForm = ProfileForm()
# 	print saved
# 	return render(request, 'saved.html', locals())
	# return render(request, 'saved.html', {'saved' : saved, 'name' : myprofile.name})

def Saveprofile(request):
	saved = False
	if request.method == "POST":
		#Get the posted form
			MyProfileForm = ProfileForm(request.POST, request.FILES)
	if MyProfileForm.is_valid():
		profile = Profile()
		profile.name = MyProfileForm.cleaned_data["name"]
		profile.picture = MyProfileForm.cleaned_data["picture"]
		profile.save()
		saved = True
	else:
		MyProfileForm = ProfileForm()
	print(saved)
	return render(request, 'saved.html', locals())

def sessionlogin(request):
	username = 'not logged in'
	if request.method == 'POST':
		MyLoginForm = LoginForm(request.POST)
		if MyLoginForm.is_valid():
			username = MyLoginForm.cleaned_data['username']
			request.session['username'] = username

	return render(request, 'loggedin.html', {'username': username})
	

def formview(request):
	if request.session.has_key('username'):
		username = request.session['username']
		return render(request, 'loggedin.html', {'username': username})
	else:
		return render(request, 'sessionlogin.html', {})

def logout(request):
	try:
		del request.session['username']
	except:
		pass
	return HttpResponse('<strong>You are Logged Out!</strong>')

# def comment(request, object_pk):
# 	mycomment = Comment.objects.get(object_pk = object_pk)
# 	text = '<p> User : %s </p>' %mycomment.user_name
# 	text+= '<p> Comment: %s</p>'%mycomment.comment
# 	return HttpResponse(text)

def comment(request, object_pk):
	mycomment = Comment.objects.get(object_pk=object_pk)
	text = '<strong>User :</strong> %s <p>'%mycomment.user_name
	text += '<strong>Comment :</strong> %s <p>'%mycomment.comment
	return HttpResponse(text)
	# return render('res.html', {'text' : text})

def dreamreal(request):
	form = Dreamrealform()
	return render(request, 'dreamreal.html', locals())








	




	


