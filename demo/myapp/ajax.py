from myapp.forms import Dreamrealform
from myapp.models import Dreamreal
from dajaxice.utils import deserialize_form
from dajax.core import Dajax

# @dajaxice_register
def send_form(request, form):
	dajax = Dajax()
	form = Dreamrealform(deserialize_form(form))
	if form.is_valid():
		dajax.remove_css_class('#my_form input', 'error')
		dr = Dreamreal()
		dr.website = form.cleaned_data.get('website')
		dr.name = form.cleaned_data.get('name')
		phone = form.cleaned_data.get('phone')
		email = form.cleaned_data.get('email')
		dr.save()
		dajax.alert('Dreamreal entry %s was successfully saved' %form.cleaned_data.get('name'))
	else:
		dajax.remove_css_class('#my_form input','error')
		for error in form.errors:
			dajax.add_css_class('#id_%s' %error, 'error')

	return dajax.json()

