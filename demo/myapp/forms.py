#-*- coding: utf-8 -*-
from django import forms
from myapp.models import Dreamreal

class LoginForm(forms.Form):
	username = forms.CharField(max_length=30)
	password = forms.CharField(widget=forms.PasswordInput())
	# For Checking whether user exists in DB
	def clean_message(self):
		username = self.cleaned_data.get("username")
		dbuser = Dreamreal.objects.filter(name = username)
		if not dbuser:
			raise forms.ValidationError('This user does not exists in Database')
		return username
	# DB check ends

class ProfileForm(forms.Form):
	name = forms.CharField(max_length=30)
	picture = forms.ImageField()

class Dreamrealform(forms.Form):
	website = forms.CharField(max_length=30)
	name = forms.CharField(max_length=30)
	phone = forms.CharField(max_length=30)
	email = forms.CharField(max_length=30)
		